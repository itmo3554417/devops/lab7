lab7

sudo docker run -d --name gitlab-runner --restart always   -v /var/run/docker.sock:/var/run/docker.sock   -v /srv/gitlab-runner/config:/etc/gitlab-runner:Z   gitlab/gitlab-runner:latest

sudo docker exec -it gitlab-runner gitlab-runner register -n         --url https://gitlab.com/       --registration-token REGISTRATION_TOKEN    --executor docker       --description "docker-build-runner"     --docker-image "docker:latest"  --docker-privileged     --docker-volumes /var/run/docker.sock:/var/run/docker.sock    --docker-pull-policy "always"   --tag-list shell-runner

sudo docker restart gitlab-runner